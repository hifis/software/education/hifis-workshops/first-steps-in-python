# SWC Python Modified

> :warning: **This project has been discontinued and is no longer maintained.**
> Please refer to the [new introductory course for Python](https://codebase.helmholtz.cloud/hifis/software/education/hifis-workshops/python-first-steps) instead.

A modified concept of the SWC python courses, adapted for our own purposes.