# Python Introduction - Setup

## Checklist

* [ ] **Make sure to run _ipython_ with a version of python 3**
* [ ] **Make sure the inflammation data has been downloaded**
* [ ] **Make sure `numpy` is installed**

## IPython

* On terminal: Call `ipython``[Enter]`
  * If not available, try `ipython3``[Enter]`
* In _ipython_: The used python version should appear in the initial prompt:
```
 >>> ipython
Python 2.7.17 (default, Nov  7 2019, 10:07:09) 
Type "copyright", "credits" or "license" for more information.
```

* In some environments, `[Ctrl] + [o]` can create a new line without executing it


