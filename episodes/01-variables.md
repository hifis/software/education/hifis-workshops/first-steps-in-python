# Python Introduction - Variables

## Overview

* Teaching: 10 min
* Exercises: 10 min
* Questions
  * How can I store data in programs?
* Objectives
  * Write programs that assign scalar values to variables and perform calculations with those values.
  * Correctly trace value changes in programs that use scalar assignment.

---

## Use Variables to Store Values

* Variables are names for values.
* In Python the `=` symbol assigns the value on the right to the name on the left.
* The variable is created when a value is assigned to it.

```python
age = 42
first_name = 'Ahmed'
```

## Variable names
* Can only contain letters, digits, and underscore `_`
* Cannot start with a digit
* Are case sensitive (`age`, `Age` and `AGE` are three different variables)

> Variable names that start with underscores like `__alistairs_real_age` have a special meaning so we won’t do that until we understand the convention.

### Python is case-sensitive.

* There are conventions
* Use lower-case letters for now.

### Use meaningful variable names.

> ```python
> flabadab = 42
> ewr_422_yY = 'Ahmed'
> print(ewr_422_yY, 'is', flabadab, 'years old')
> ```

* Use meaningful variable names to help other people understand what the program does.
  * The most important “other person” is your future self.

## Use Print to Display Values.

> _IPython_ automatically prints the result of a line if it is not assigned to something
> This is nice for exploration, but will not always work the same in other tools

* Built-in function `print` that prints things as text.
* You know functions from mathmatics: Remember `y = f(x)`
* Call the function (i.e., tell Python to run it) by using its name.
* Provide values to the function (i.e., the things to print) in parentheses.
* To add a string to the printout, wrap the string in single or double quotes.
* The values passed to the function are called arguments

```python
print(first_name, 'is', age, 'years old')
```

* `print` automatically puts a single space between items to separate them…
* … and wraps around to a new line at the end.

## Variables Must be Created Before They are Used.

* If a variable doesn’t exist yet, or if the name has been mis-spelled, Python reports an error. 
* (Unlike some languages, which “guess” a default value.)

```python
print(last_name)
```
```
---------------------------------------------------------------------------
NameError                                 Traceback (most recent call last)
<ipython-input-1-c1fbb4e96102> in <module>()
----> 1 print(last_name)

NameError: name 'last_name' is not defined
```

* The last line of an error message is usually the most informative.

## Variables can be Used in Calculations.

* We can use variables in calculations just as if they were values.

> Remember, we assigned the value 42 to age a few lines ago.

```python
age = age + 3
print('Age in three years:', age)
```

## Use an Index to get a Single Character From a String.

```python
atom_name = 'helium'
```

* The characters (individual letters, numbers, and so on) in a string are ordered. 
  * For example, the string 'AB' is not the same as 'BA'. 
  * Because of this ordering, we can treat the string as a list of characters.
* Each position in the string (first, second, etc.) is given a number.
* Indices are numbered from 0.
  * Use the position’s index in square brackets to get the character at that position.

| 0 | 1 | 2 | 3 | 4 | 5 |
|---|---|---|---|---|---|
| h | e | l | i | u | m |


```python
print(atom_name[0])
```

## Use a Slice to get a Substring.

* A part of a string is called a substring. 
  * A substring can be as short as a single character.
* An item in a list is called an element. 
  * Whenever we treat a string as if it were a list, the string’s elements are its individual characters.
* A slice is a part of a string (or, more generally, any list-like thing).
* We take a slice by using `[start:stop]`, 
  * `start` is replaced with the index of the first element we want
  * `stop` is replaced with the index of the element just after the last element we want.
  * Mathematically, you might say that a slice selects `[start:stop)`.
  * The difference between stop and start is the slice’s length.
* Taking a slice does *not* change the contents of the original string. 
  * Instead, the slice is a copy of part of the original string.

```python
print(atom_name[0:2])
```

## Use `len(…)` to obtain the Length of a String

```python
letter_count = len(atom_name)
print("The word", atom_name, "has", letter_count, "letters")
```

## Special Case: `None`
* `None` is a **value** for things that are defined but have no explicit value
  * Example: The result of a `print(…)` statement

```python
underscores = "_____"
has_value = len(underscores)
has_no_value = print(underscores)

print(has_value)
print(has_no_value)
```
```
_____ 
5
None
```

## Use comments to add documentation to programs.

```python
# This sentence isn't executed by Python.
adjustment = 0.5   # Neither is this - anything after '#' is ignored.
```

# Key Points

* Use variables to store values.
* Python is case-sensitive.
* Use meaningful variable names.
* Use print to display values.
* Variables must be created before they are used.
* Variables can be used in calculations.
* Use an index to get a single character from a string.
* Use a slice to get a substring.
* Use `len` to obtain the length of a string
* There is the special value `None`
* Comments can be used to document code
