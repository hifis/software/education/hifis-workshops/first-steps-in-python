# Python Introduction - Libraries


## Overview
* Teaching: 10 min
* Exercises: 10 min
* Questions
  * How can I use software that other people have written?
  * How can I find out what that software does?
* Objectives
  * Explain what software libraries are and why programmers create and use them.
  * Write programs that import and use libraries from Python’s standard library.
  * Find and read documentation for standard libraries interactively (in the interpreter) and online.

## Most of the power of a programming language is in its libraries.

* A library is a collection of files (called modules) that contains functions for use by other programs.
  * May also contain data values (e.g., numerical constants) and other things.
  * Library’s contents are supposed to be related, but there’s no way to enforce that.
* The Python standard library is an extensive suite of modules that comes with Python itself.
* Many additional libraries are available from PyPI (the Python Package Index).
* We will see later how to write new libraries.

## Libraries and modules

* A library is a collection of modules, but the terms are often used interchangeably, especially since many libraries only consist of a single module, so 
  * Don’t worry if you mix them.

## A program must import a library module before using it.

* Use `import` to load a library module into a program’s memory.
  * Then refer to things from the module as `module_name.thing_name`.
    * Python uses `.` to mean “part of”.
* Using math, one of the modules in the standard library:

```python
import math

print('pi is', math.pi)
print('cos(pi) is', math.cos(math.pi))
```

> It is good practise to put the imports at the top of a program

# Have to refer to each item with the module’s name.
* `math.cos(pi)` won’t work: the reference to `pi` doesn’t somehow “inherit” the function’s reference to `math`.

# Use help to learn about the contents of a library module.

*  Works just like help for a function.

```python
help(math)
```

# Import specific items from a library module to shorten programs.

* Use `from … import …` to load only specific items from a library module.
  * Then refer to them directly without library name as prefix.

```python
from math import cos, pi

print('cos(pi) is', cos(pi))
```

## Create an alias for a library module when importing it to shorten programs.

* Use import ... as ... to give a library a short alias while importing it.
  * Then refer to items in the library using that shortened name.

```python
import math as m

print('cos(pi) is', m.cos(m.pi))
```

* Commonly used for libraries that are frequently used or have long names.
  * E.g., matplotlib plotting library is often aliased as mpl.
  * But can make programs harder to understand, since readers must learn your program’s aliases.

#  Key Points

* Most of the power of a programming language is in its libraries.
* A program must import a library module in order to use it.
* Use help to learn about the contents of a library module.
* Import specific items from a library to shorten programs.
* Create an alias for a library when importing it to shorten programs.

