# Python Introduction - Conditionals

## Overview
* Teaching: 10 min
* Exercises: 15 min
* Questions
  * How can programs do different things for different data?
* Objectives
  * Correctly write programs that use if and else statements and simple Boolean expressions (without logical operators).
  * Trace the execution of unnested conditionals and conditionals inside loops.

## Use `if` statements to control whether or not a block of code is executed.
* An `if` statement (more properly called a conditional statement) controls whether some block of code is executed or not.
  * Structure is similar to a `for` statement:
  * First line opens with if and ends with a colon
  * Body containing one or more statements is indented (usually by 4 spaces)

```python
mass = 3.54
if mass > 3.0:
    print(mass, 'is large')

mass = 2.07
if mass > 3.0:
    print (mass, 'is large')
```

* The _condition_ must eveluate to `True` or `False`
* Since `=` is used for assignment `==` is used for equality

## Conditionals are often used inside loops.
* Not much point using a conditional when we know the value (as above).
  * But useful when we have a collection to process.

```python
masses = [3.54, 2.07, 9.22, 1.86, 1.71]
for mass in masses:
    if mass > 3.0:
        print(m, 'is large')
```

## Use `else` to execute a block of code when an if condition is not true.
* `else` can be used following an `if`.
* Allows us to specify an alternative to execute when the `if` branch isn’t taken.

```python
masses = [3.54, 2.07, 9.22, 1.86, 1.71]
for m in masses:
    if m > 3.0:
        print(m, 'is large')
    else:
        print(m, 'is small')
```

## Use `elif` to specify additional tests.

* May want to provide several alternative choices, each with its own test.
* Use `elif` (short for “else if”) and a condition to specify these.
* Always associated with an if.
* Must come before the `else` (which is the “catch all”).

```python
masses = [3.54, 2.07, 9.22, 1.86, 1.71]
for m in masses:
    if m > 9.0:
        print(m, 'is HUGE')
    elif m > 3.0:
        print(m, 'is large')
    else:
        print(m, 'is small')
```

## Conditions are tested once, in order.

* **Ordering matters.**

```python
grade = 85
if grade >= 70:
    print('grade is C')
elif grade >= 80:
    print('grade is B')
elif grade >= 90:
    print('grade is A')
```

* Does not automatically go back and re-evaluate if values change.

```python
velocity = 10.0
if velocity > 20.0:
    print('moving too fast')
else:
    print('speed up')
    velocity = velocity + 10.0
```

* Often use conditionals in a loop to “evolve” the values of variables.

```python
velocity = 10.0
for i in range(5): # execute the loop 5 times
    print(i, ':', velocity)
    if velocity > 20.0:
        print('moving too fast')
        velocity = velocity - 5.0
    else:
        print('moving too slow')
        velocity = velocity + 10.0
print('final velocity:', velocity)
```

## Combined Conditions

* Conditions can be combined with `not …`, `… and …` and `… or …`
* Use parenthesis to structure logical expressions

```python
# Let's guess the animal
is_mammal = True
can_jump = False
can_fly = False
can_walk = True

if not is_mammal:
    print("I have no idea")

if can_fly or can_walk:
    print("It is neither whale nor dolphin")

if is_mammal and can_fly:
    print("It could be a bat")

# Logic operations can be combined
if can_jump and not (can_walk or can_fly):
    print("Are you sure?")

if can_walk and not can_jump and not can_fly:
    print("It's an elephant!")
```

# Key Points

* Use `if` statements to control whether or not a block of code is executed.
* Conditionals are often used inside loops.
* Use `else` to execute a block of code when an `if` condition is not true.
* Use `elif` to specify additional tests.
* Conditions are tested once, in order.
* Create a table showing variables’ values to trace a program’s execution.
* Logical conditions can be combined


