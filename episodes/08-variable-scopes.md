# Python Introduction - Variable Scopes


## Overview
* Teaching: 10 min
* Exercises: 10 min
* Questions
  * How do function calls actually work?
  * How can I determine where errors occurred?
* Objectives
  * Identify local and global variables.
  * Identify parameters as local variables.
  * Read a traceback and determine the file, function, and line number on which the error occurred, the type of error, and the error message.

## The scope of a variable is the part of a program that can ‘see’ that variable.

* There are only so many sensible names for variables.
* People using functions shouldn’t have to worry about what variable names the author of the function used.
* People writing functions shouldn’t have to worry about what variable names the function’s caller uses.
* The part of a program in which a variable is visible is called its scope.

```python
pressure = 103.9

def adjust(t):
    temperature = t * 1.43 / pressure
    return temperature
```

* `pressure` is a global variable.
  * Defined outside any particular function.
  * Visible everywhere.
* `t` and `temperature` are local variables in adjust.
  * Defined in the function.
  * Not visible in the main program.
  * Remember: a function parameter is a variable that is automatically assigned a value when the function is called.

```python
print('adjusted:', adjust(0.9))
print('temperature after call:', temperature)
```
```
adjusted: 0.01238691049085659

Traceback (most recent call last):
  File "/Users/swcarpentry/foo.py", line 8, in <module>
    print('temperature after call:', temperature)
NameError: name 'temperature' is not defined
```


# Key Points

* The scope of a variable is the part of a program that can ‘see’ that variable.


