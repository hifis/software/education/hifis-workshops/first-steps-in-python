# NumPy - Visualizing Data

## Overview
* Teaching: 30 min
* Exercises: 20 min
* Questions
  * How can I visualize tabular data in Python?
  * How can I group several plots together?
* Objectives
  * Plot simple graphs from data.
  * Group several graphs in a single figure.

## Visualizing data

* `matplotlib` library/module
  * Has a submodule `pyplot` for plotting data

```python
from matplotlib import pyplot
image = pyplot.imshow(data)
pyplot.show()
```

![Example Visualization from Carpentries](https://swcarpentry.github.io/python-novice-inflammation/fig/inflammation-01-imshow.svg)

```python
average_plot = pyplot.plot(numpy.mean(data, axis=0))
pyplot.show()
```

* **Note the difference between the functions used!**
  * `plot()` vs `imshow()`

```python
max_plot = pyplot.plot(numpy.max(data, axis=0))
pyplot.show()

min_plot = pyplot.plot(numpy.min(data, axis=0))
pyplot.show()
```

## Grouping plots

* Group similar plots in a single figure using subplots
* `matplotlib.pyplot.figure()` creates a space into which we will place all of our plots
  * `figsize` tells how big to make this space
* Each subplot is placed into the figure using its `add_subplot`
  * `add_subplot` method takes 3 parameters: rows, columns, 
    * The final parameter denotes which subplot your variable is referencing (left-to-right, top-to-bottom). 
    * Each subplot is stored in a different variable (`subplot0`, `subplot1`, `subplot2`). 
    * Once a subplot is created, the axes can be titled using the set_xlabel() command (or set_ylabel()).

> **Do the following in a file, so it can be copied and modified for the exercises**

```python
import numpy
import matplotlib.pyplot

data = numpy.loadtxt(fname='inflammation-01.csv', delimiter=',')

figure = matplotlib.pyplot.figure(figsize=(10.0, 3.0))

# We could keep the subplots in a list as well…
subplot0 = figure.add_subplot(1, 3, 1)
subplot1 = figure.add_subplot(1, 3, 2)
subplot2 = figure.add_subplot(1, 3, 3)

subplot0.set_ylabel('average')
subplot0.plot(numpy.mean(data, axis=0))

subplot1.set_ylabel('max')
subplot1.plot(numpy.max(data, axis=0))

subplot2.set_ylabel('min')
subplot2.plot(numpy.min(data, axis=0))

# Set a layout for better looks
figure.tight_layout()

matplotlib.pyplot.show()
```

#  Key Points
* Use the pyplot module from the matplotlib library for creating simple visualizations.

