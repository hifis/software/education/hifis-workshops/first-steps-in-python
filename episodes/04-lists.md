# Python Introduction - Lists

## Overview
* Teaching: 10 min
* Exercises: 10 min
* Questions
  * How can I store multiple values?
* Objectives
  * Explain why programs need collections of values.
  * Write programs that create flat lists, index them, slice them, and modify them through assignment and method calls.

## A list stores many values in a single structure.
* Doing calculations with a hundred variables called `pressure_001`, `pressure_002`, etc., would be at least as slow as doing them by hand.
* Use a list to store many values together.
  * Contained within square brackets `[...]`.
  * Values separated by commas ,.
* Use `len` to find out how many values are in a list.

```python
pressures = [0.273, 0.275, 0.277, 0.275, 0.276]
print('pressures:', pressures)
print('length:', len(pressures))
```

## Use an item’s index to fetch it from a list.
* Just like strings.

```python
print('zeroth item of pressures:', pressures[0])
print('fourth item of pressures:', pressures[4])
```

## Lists’ values can be replaced by assigning to them.
* Use an index expression on the left of assignment to replace a value.

```python
pressures[0] = 0.265
print('pressures is now:', pressures)
```

## Appending items to a list lengthens it.
* Use `list_name.append` to add items to the end of a list.

```python
primes = [2, 3, 5]
print('primes is initially:', primes)
primes.append(7)
primes.append(9)
print('primes has become:', primes)
```

* `append` is a _method_ of lists.
  * Like a function, but tied to a particular object.
* Use `object_name.method_name` to call methods.

* `extend` is similar to append, but it allows you to combine two lists.

```python
teen_primes = [11, 13, 17, 19]
middle_aged_primes = [37, 41, 43, 47]
print('primes is currently:', primes)

# Use `extend`
primes.extend(teen_primes)
print('primes has now become:', primes)

# Use `append`
primes.append(middle_aged_primes)
print('primes has finally become:', primes)
```
* **NOTE the difference resulting from the two methods**

## Use del to remove items from a list entirely.

* `del list_name[index]` removes an item from a list and shortens the list.
* Not a function or a method, but a statement in the language.

```python
primes = [2, 3, 5, 7, 9]
print('primes before removing last item:', primes)
del primes[4]
print('primes after removing last item:', primes)
```
## The empty list contains no values.
* Use `[]` on its own to represent a list that doesn’t contain any values.
  * “The zero of lists.”
* Helpful as a starting point for collecting values

## Lists may contain values of different types.
* A single list may contain numbers, strings, and anything else.

```python
goals = [1, 'Create lists.', 2, 'Extract items from lists.', 3, 'Modify lists.']
```

## Reminder: Character strings can be indexed like lists.
* Get single characters from a character string using indexes in square brackets.

```python
element = 'carbon'
print('zeroth character:', element[0])
print('third character:', element[3])
```

## But: Character strings are immutable.

* Cannot change the characters in a string after it has been created.
  * Immutable: can’t be changed after creation.
  * In contrast, lists are mutable: they can be modified in place.
* Python considers the string to be a single value with parts, not a collection of values.

```python
element[0] = 'C'
```
## Indexing beyond the end of the list is an error.
* Python reports an `IndexError` if we attempt to access a value that doesn’t exist.
  * This is a kind of runtime error.
  * Cannot be detected as the code is parsed because the index might be calculated based on data.

```python
print('99th element of element is:', element[99])
```

# Key Points
* A list stores many values in a single structure.
* Use an item’s index to fetch it from a list.
* Lists’ values can be replaced by assigning to them.
* Appending items to a list lengthens it.
* Use del to remove items from a list entirely.
* The empty list contains no values.
* Lists may contain values of different types.
* Character strings can be indexed like lists.
* Character strings are immutable.
* Indexing beyond the end of the collection is an error.






