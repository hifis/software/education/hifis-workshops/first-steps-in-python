# Python Introduction - Data Types

## Overview
* Teaching: 10 min
* Exercises: 10 min
* Questions
  * What kinds of data do programs store?
  * How can I convert one type to another?
* Objectives
  * Explain key differences between integers and floating point numbers.
  * Explain key differences between numbers and character strings.
  * Use built-in functions to convert between integers, floating point numbers, and strings.

---

## Every Value has a Type

* Every value in a program has a specific type.
* Integer (int): represents positive or negative whole numbers like 3 or -512.
* Floating point number (float): represents real numbers like 3.14159 or -2.5.
* Truth values (Boolean values)
  * Can be `True` or `False`
* Character string (usually called “string”, str): text.
  * Written in either single quotes or double quotes (as long as they match).
  * The quote marks aren’t printed when the string is displayed.

## Use the Built-in Function `type` to Find the Type of a Value

* Use the built-in function `type` to find out what type a value has.
  * Works on variables as well.
  * But remember: the value has the type — the variable is just a label.

**Python**
```python
print(type(52))

fitness = 'average'
print(type(fitness))
```
```
<class 'int'>
<class 'str'>
```

## Types Control What Operations (or Methods) can be Performed on a Given Value

* A value’s type determines what the program can do to it.

**Python**
```python
print(5 - 3)
```
```
2
```

**Python**
```python
print('hello' - 'h')
```
```
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-2-67f5626a1e07> in <module>()
----> 1 print('hello' - 'h')

TypeError: unsupported operand type(s) for -: 'str' and 'str'
```

## You can use the “+” and “*” Operators on Strings

* “Adding” character strings concatenates them.

**Python**
```python
full_name = 'Ahmed' + ' ' + 'Walsh'
print(full_name)
```
```
Ahmed Walsh
```

* Multiplying a character string by an integer `N` creates a new string that consists of that character string repeated `N` times.
  * Since multiplication is repeated addition.

**Python**
```python
separator = '=' * 10
print(separator)
```
```
==========
```

## Strings Have a Length (but Numbers don’t)

* The built-in function `len` counts the number of characters in a string.

**Python**
```python
print(len(full_name))
```
```
11
```

* But numbers don’t have a length (not even zero).

**Python**
```python
print(len(52))
```
```

---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-3-f769e8e8097d> in <module>()
----> 1 print(len(52))

TypeError: object of type 'int' has no len()
```

* Must convert numbers to strings or vice versa when operating on them.
  * Cannot add numbers and strings.

**Python**
```python
print(1 + '2')
```
```
---------------------------------------------------------------------------
TypeError                                 Traceback (most recent call last)
<ipython-input-4-fe4f54a023c6> in <module>()
----> 1 print(1 + '2')

TypeError: unsupported operand type(s) for +: 'int' and 'str'
```

* Not allowed because it’s ambiguous: should 1 + '2' be 3 or '12'?

## Some Types can be Converted to Other Types by Using the Type Name as a Function

**Python**
```python
print(1 + int('2'))
print(str(1) + '2')
```
```
3
12
```

## Can mix Integers and Floats Freely in Operations

* Integers and floating-point numbers can be mixed in arithmetic.
  * Python 3 automatically converts integers to floats as needed. 
> (Integer division in Python 2 will return an integer, the floor of the division.)

**Python**
```python
print('half is', 1 / 2.0)
print('three squared is', 3.0 ** 2)
```
```
half is 0.5
three squared is 9.0
```

## Variables Only Change Value when Something is Assigned to Them

If we make one cell in a spreadsheet depend on another, and update the latter, the former updates automatically.
This does not happen in programming languages.

**Python**
```python
first = 1
second = 5 * first
first = 2
print('first is', first, 'and second is', second)
```
```
first is 2 and second is 5
```

The computer reads the value of first when doing the multiplication, creates a new value, and assigns it to second.
After that, second does not remember where it came from.

# Key Points

* Every value has a type.
* Use the built-in function `type` to find the type of a value.
* Types control what operations can be done on values.
* Strings can be added and multiplied.
* Strings have a length (but numbers don’t).
* Must convert numbers to strings or vice versa when operating on them.
* Can mix integers and floats freely in operations.
* Variables only change value when something is assigned to them.
