## Episode 1 - Variables

### Individual Exercises

#### Exercise 0: Naming Variables
Which is a better variable name, `m`, `min`, or `minutes`? Why? 
_Hint:_ think about which code you would rather inherit from someone who is leaving the lab:

1) `ts = m * 60 + s`
2) `tot_sec = min * 60 + sec`
3) `total_seconds = minutes * 60 + seconds`

##### Post your solutions:

*
*

---

#### Exercise 1.1: Predicting Values

What is the final value of `position` in the program below? (Try to predict the value without running the program, then check your prediction.)

```python
initial = 'left'
position = initial
initial = 'right'
```

##### Post your solutions:

*
*

---

#### Exercise 1.2: A Challenge

If you assign `a = 123`, what happens if you try to get the second digit of `a` via `a[1]`?

##### Post your solutions:

*
*

---

#### Exercise 1.3: Slicing

What does the following program print?

```python
atom_name = 'carbon'
print("atom_name[1:3] is:", atom_name[1:3])
```

##### Post your solutions:

*
*

---
### Breakout Rooms

#### Exercise 1.4: Swapping Values
Fill the table showing the values of the variables in this program after each statement is executed.
Use a `—` for _undefined_ or _unknown_

```python
# Command  # Value of x   # Value of y   # Value of swap #
x = 1.0    #              #              #               #
y = 3.0    #              #              #               #
swap = x   #              #              #               #
x = y      #              #              #               #
y = swap   #              #              #               #
```

##### Answers from Breakout Rooms:
###### Room 1
```python
# Command  # Value of x   # Value of y   # Value of swap #
x = 1.0    #              #              #               #
y = 3.0    #              #              #               #
swap = x   #              #              #               #
x = y      #              #              #               #
y = swap   #              #              #               #
```

###### Room 2
```python
# Command  # Value of x   # Value of y   # Value of swap #
x = 1.0    #              #              #               #
y = 3.0    #              #              #               #
swap = x   #              #              #               #
x = y      #              #              #               #
y = swap   #              #              #               #
```
###### Room 3
```python
# Command  # Value of x   # Value of y   # Value of swap #
x = 1.0    #              #              #               #
y = 3.0    #              #              #               #
swap = x   #              #              #               #
x = y      #              #              #               #
y = swap   #              #              #               #
```

#### Exercise 1.5: Slicing (Advanced)

Assume `thing` is a string and `low`, `high`, `number` are positive integer values, while `some-negative-number` is a negative integer value:

1) What does `thing[low:high]` do?
2) What does `thing[low:]` (without a value after the colon) do?
3) What does `thing[:high]` (without a value before the colon) do?
4) What does `thing[:]` (just a colon) do?
5) What does `thing[number:some-negative-number]` do?
6) What happens when you choose a high value which is out of range? 
    * (e.g., try `atom_name[0:15]`)

##### Answers from Breakout Rooms:

###### Room 1
1)
2)
3)
4)
5)
6)

###### Room 2
1)
2)
3)
4)
5)
6)

###### Room 3
1)
2)
3)
4)
5)
6)
