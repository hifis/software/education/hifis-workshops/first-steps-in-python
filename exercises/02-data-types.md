## Episode 2 - Data Types

### Individual Exercises

#### Exercise 2.1: Fractions

What type of value is 3.4? How can you find out?

##### Post your solutions:

*
*

#### Exercise 2.2: Automatic Type Conversion

What type of value is 3.25 + 4?

##### Post your solutions:

*
*

#### Exercise 2.3: Arithmetic with Different Types

Which of the following will return the floating point number 2.0? 
**Note:** there may be more than one right answer.

```python
first = 1.0
second = "1"
third = "1.1"
```

1. `first + float(second)`
2. `float(second) + float(third)`
3. `first + int(third)`
4. `first + int(float(third))`
5. `int(first) + int(float(third))`
6. `2.0 * second`

##### Post your solutions:

*
*

### Breakout Rooms

#### Exercise 2.4: Choose a Type

What type of value (`integer`, `floating point number`, or `character string`) would you use to represent each of the following? 
Try to come up with more than one good answer for each problem. 
For example, in #1, when would counting days with a floating point variable make more sense than using an integer?

1. Number of days since the start of the year.
2. Time elapsed from the start of the year until now in days.
3. Serial number of a piece of lab equipment.
4. A lab specimen’s age
5. Current population of a city.
6. Average population of a city over time.

##### Answers from Breakout Rooms:
###### Room 1
1)
2)
3)
4)
5)
6)

###### Room 2
1)
2)
3)
4)
5)
6)

###### Room 3
1)
2)
3)
4)
5)
6)
