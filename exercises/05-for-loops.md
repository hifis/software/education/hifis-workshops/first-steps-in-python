## Episode 5  - For-Loops
### Individual Exercises

#### Exercise 5.1 - Tracing Execution

Create a table showing the numbers of the lines that are executed when this program runs, and the values of the variables after each line is executed.
```python
total = 0             # Line 1
for char in "tin":    # Line 2
    total = total + 1 # Line 3
```
:arrow_down: You can copy this table for your answer :arrow_down:
| Line | `total` = | `char` = |
|------|-----------|----------|
|      |           |          |
|      |           |          |
|      |           |          |
|      |           |          |
|      |           |          |
|      |           |          |
|      |           |          |


##### Post your solutions:



#### Exercise 5.2 - Reversing a String

Fill in the blanks in the program below so that it prints _“nit”_ (the reverse of the original character string _“tin”_).

```python
original = "tin"
result = ____
for char in original:
    result = ____
print(result)
```

##### Post your solutions:


#### Exercise 5.3 - Practice Accumulating

Fill in the blanks in each of the programs below to produce the indicated result.

1) Total length of the strings in the list: `["red", "green", "blue"]` => `12`
```python
total = 0
for word in ["red", "green", "blue"]:
    ____ = ____ + len(word)
print(total)
```
2) List of word lengths: `["red", "green", "blue"]` => `[3, 5, 4]`
```python
lengths = ____
for word in ["red", "green", "blue"]:
    lengths.____(____)
print(lengths)
```
3) Concatenate all words: `["red", "green", "blue"]` => `"redgreenblue"`
```python
words = ["red", "green", "blue"]
result = ____
for ____ in ____:
    ____
print(result)
```
4) Create an acronym: `["red", "green", "blue"]` => `"RGB"`
    Write the whole program

##### Post your solutions:

#### Exercise 5.4 - Identifying Item Errors

1) Read the code below and try to identify what the errors are without running it.
2) Run the code, and read the error message. What type of error is it?
3) Fix the error.
    
```python
seasons = ['Spring', 'Summer', 'Fall', 'Winter']
print('My favorite season is ', seasons[4])
```

##### Post your solutions:

*
*
