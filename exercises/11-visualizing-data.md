## Episode 11 - Visualizing Data

### Individual Exercises

#### Exercise 11.1 - Plot Scaling

Why do all of our plots stop just short of the upper end of our graph?
If we want to change this, we can use the `set_ylim(min, max)` method of each _subplot_, for example:

```python
subplot0.set_ylim(0,6)
```
Update your plotting code to automatically set a more appropriate scale. (Hint: you can make use of the `max` and `min` methods to help.)


#### Exercise 11.2 - Drawing Straight Lines

In the center and right subplots above, we expect all lines to look like step functions because non-integer value are not realistic for the minimum and maximum values. However, you can see that the lines are not always vertical or horizontal, and in particular the step function in the subplot on the right looks slanted. 

* Why is this?

 One way to do avoid this is to use the Matplotlib `drawstyle` option.
 You can check the [respective matplotlib manual page](https://matplotlib.org/3.1.1/api/_as_gen/matplotlib.pyplot.plot.html) regarding possible values for this option.

 * Modify your code to output a more stair-like plots like this:

 ![Carpentries example solution image](https://swcarpentry.github.io/python-novice-inflammation/fig/inflammation-01-line-styles.svg)

#### Exercise 11.3 - Make Your Own Plot

Create a plot showing the standard deviation (`numpy.std(…)`) of the inflammation data for each day across all patients.


#### Exercise 11.4 - Moving Plots Around

Modify the program to display the three plots on top of one another instead of side by side.
