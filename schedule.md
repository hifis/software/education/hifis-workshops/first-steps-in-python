# Python Lesson Plan

## Requirements

Basic available time is 8h (= 480 min).
* 60 min for Breaks
* 20 min Setup
* 20 min Wrap-Up
* Leaves 380 min pure lesson time

1. Variables
  * 10 min + 10 min
  * https://swcarpentry.github.io/python-novice-gapminder/02-variables/index.html
2. Data Types
  * 10 min + 10 min
  * https://swcarpentry.github.io/python-novice-gapminder/03-types-conversion/index.html
3. Built-in Functions
  * 15 min + 10 min
  * https://swcarpentry.github.io/python-novice-gapminder/04-built-in/index.html
4. Lists
  * 10 min + 10 min
  * Dependencies: **data types**, **built-in functions**
  * https://swcarpentry.github.io/python-novice-gapminder/11-lists/index.html
5. For Loops
  * 10 min + 10 min
  * Dependencies: **built-in functions**, **lists**
  * https://swcarpentry.github.io/python-novice-gapminder/12-for-loops/index.html
6. Conditionals
  * 10 min + 15 min
  * Depenedncies: **lists**, **built-in functions**, **for-loops**
  * https://swcarpentry.github.io/python-novice-gapminder/17-conditionals/index.html
7. Writing Functions
  * 10 min + 15 min
  * Dependencies: **built-in-functions**, **variables**
  * https://swcarpentry.github.io/python-novice-gapminder/14-writing-functions/index.html
8. Variable Scopes
  * 10 min + 10 min
  * Dependencies: **functions**
  * https://swcarpentry.github.io/python-novice-gapminder/15-scope/index.html
9. Libraries
  * 10 min + 10 min
  * https://swcarpentry.github.io/python-novice-gapminder/06-libraries/index.html

---
The following are taken from the _Inflammation_ - lesson in order.

10. Analyzing Patient Data
  * 30 min + 20 min 
  * Reduced time estimate by 10 min, since topics were covered before
  * https://swcarpentry.github.io/python-novice-inflammation/02-numpy/index.html
11. Visualizing Tabular Data
  * 30 min + 20 min
  * https://swcarpentry.github.io/python-novice-inflammation/03-matplotlib/index.html
12. Analyzing Data From Multiple Files
  * 20 min
  * https://swcarpentry.github.io/python-novice-inflammation/06-files/index.html
13. Defensive Programming
  * Only _Assertions_
  * 15 min
  * https://swcarpentry.github.io/python-novice-inflammation/10-defensive/index.html
14. _(Optional) Debugging_
  * Only First Exercise
  * 0 min + 15 min
  * https://swcarpentry.github.io/python-novice-inflammation/11-debugging/index.html

---

360 (+15) min runtime
